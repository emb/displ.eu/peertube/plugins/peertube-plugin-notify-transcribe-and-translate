import { createReadStream } from 'fs-extra';
import { MVideoCaptionVideo, MVideoFullLight } from '@peertube/peertube-types/server/types/models';
import { PeerTubeHelpers } from '@peertube/peertube-types/server/types/plugins/register-server-option.model';
import { VideoUpdate, PluginSettingsManager } from '@peertube/peertube-types';
import { LoggerInterface } from '../Model';
import { AuthenticatedPeerTubeResponse, PeerTubeApi } from '../PeerTube';
import { TranscriptionWorkerPool } from '../Transcription/TranscriptionWorkerPool';
import { LanguageModelManager } from '../Recognizer';
import { TranscriptionRequest } from '../Transcription/TranscriptionRequest';
import { MVideo } from '@peertube/peertube-types/server/types/models/video/video';
import fs from 'fs';
const { mkdir, writeFile } = require('fs/promises');
const { Readable } = require('stream');
const { finished } = require('stream/promises');

const path = require('path');
import { exec } from 'child_process';

interface VideoUpdatedApiActionParameters {
  video: MVideoFullLight;
  body: VideoUpdate;
  req: Express.Request;
  res: AuthenticatedPeerTubeResponse;
}

export class VideoController {
  private readonly logger: LoggerInterface;
  private languageModelManager: LanguageModelManager;
  private peerTubeApi: PeerTubeApi;
  private workerPool: TranscriptionWorkerPool;
  private peerTubeVideosHelpers: PeerTubeHelpers['videos'];
  private settingsManager: PluginSettingsManager;

  constructor(
    logger: LoggerInterface,
    languageModelManager: LanguageModelManager,
    peerTubeApi: PeerTubeApi,
    peerTubeVideosHelpers: PeerTubeHelpers['videos'],
    workerPool: TranscriptionWorkerPool,
    settingsManager: PluginSettingsManager
  ) {
    this.logger = logger;
    this.languageModelManager = languageModelManager;
    this.peerTubeApi = peerTubeApi;
    this.peerTubeVideosHelpers = peerTubeVideosHelpers;
    this.workerPool = workerPool;
    this.settingsManager = settingsManager;
  }

  /**
   * This event is fired when the video is `uploaded`, there isn't a `created` event.
   * The video creation form actually trigger an `updated` event (see below).
   *
   * But this might be useful to trigger transcription sooner if we choose to force transcription
   * without a user-defined language.
   */
  async uploaded() {}

  async updated({ video, res }: VideoUpdatedApiActionParameters) {
    const settings = await this.settingsManager.getSettings(['trigger-transcription-and-translation']);
    if (!settings['trigger-transcription-and-translation']) {
      this.logger.error('trigger-transcription-and-translation setting is missing');
    }

    const { language, id } = video;
    this.logger.debug('Updating video', { id, language });

    this.peerTubeApi.setOAuthTokenFromResponse(res);
    const alreadyHasCaption = await this.peerTubeApi.hasVideoCaptionForLanguage(id, language);
    if (alreadyHasCaption) {
      this.logger.info(`Video "${id}" already have a caption for "${language}", skipping.`);
    }

    if (!language) {
      this.logger.info(`Video "${id}" doesn't have a language defined.`);
    }

    const videoFiles = await this.getVideoFiles(id);
    if (videoFiles.length === 0) {
      this.logger.info(`No video file found for video "${id}", skipping.`);
      return;
    }

    this.logger.info(`Sending transcription request with ID "${id}" in "${language}" to Whisper Service Connector`);

    if (videoFiles && videoFiles.length > 0 && videoFiles[0].url) {
      this.logger.error('Chagai testing', videoFiles[0]);
      var url = videoFiles[0].url; // TODO get the lowest video quality
      var videoFilePath = videoFiles[0].path;
      this.logger.info(`${url} this url is saved in videoFiles[0]`);

      async function fetchWebVideoDownloadLocally(url: string) {
        const downloadFile = async (url: string, folder = '.') => {
          const res = await fetch(url);
          if (!fs.existsSync('downloads')) await mkdir('downloads'); //Optional if you already have downloads directory
          const destination = path.resolve('./downloads', folder);
          const fileStream = fs.createWriteStream(destination, { flags: 'wx' });
          await finished(Readable.fromWeb(res.body).pipe(fileStream));
        };

        downloadFile(url, '/home/chagai/peertube-plugin-transcription/test4.mp4'); //TODO look for valid path, filename tmp
      }

      async function copyWebVideoToTmp(videoFilePath: string) {
        //0 alternativ

        //1 alternativ:
        fs.promises
          .copyFile(`${path}`, `/tmp/${id}.mp4'`)
          .then(() => console.log(`${path} is copied to /tmp/${id}.mp4'`))
          .then(() => console.log(`Flow upload file to Mock!!`))
          .catch(() => console.log('The file could not be copied'));

        //2 alternativ:
        exec(`cp -f '${path}' '/tmp/${id}.mp4'`, (error: any, stdout: any, stderr: any) => {
          if (error) {
            console.log(`error: ${error.message}`);
            return;
          }
          if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
          }
          console.log(`stdout: ${stdout}`);

          //Service observes directory and process will be started and push it to the server/ fetch it in the server
          //Json noch dazuuu holen? um id zuordnen zu können?
          //oder innerhalb des plugins?
        });
      }

      //not transcoded
      if (url.includes('web-videos')) {
        this.logger.error('Chagai testing web-videos');
        // copyWebVideoToTmp(videoFilePath);
        // fetchWebVideoDownloadLocally(url);
        this.triggerTranscriptionAndTranslation(
          video.id,
          url,
          language,
          settings['trigger-transcription-and-translation']
        );
      }

      //transcoded
      if (url.includes('streaming-playlist')) {
        this.logger.error('Chagai testing streaming-playlist');
        this.logger.error('Chagai testing file dont has to be saved');
        this.triggerTranscriptionAndTranslation(
          video.id,
          url,
          language,
          settings['trigger-transcription-and-translation']
        );
      }
    } else {
      this.logger.error('videoFiles does not have a valid URL.');
    }
  }

  async captionDeleted({
    caption,
    res,
  }: {
    caption: MVideoCaptionVideo;
    req: Express.Request;
    res: AuthenticatedPeerTubeResponse;
  }) {
    const video = await this.peerTubeVideosHelpers.loadByIdOrUUID(caption.videoId);

    if (!video.language) {
      this.logger.info(`Video "${video.id}" doesn't have a language set. Skipping transcription...`);
      return;
    }

    if (video.language !== caption.language) {
      this.logger.info(
        `Deleted caption was in ${caption.language} whereas video is in ${video.language}. Skipping transcription...`
      );
      return;
    }

    const videoFiles = await this.getVideoFiles(video.id);
    if (videoFiles.length === 0) {
      this.logger.info(`No valid video file found for video "${video.id}". Skipping transcription...`);
      return;
    }

    const modelName = await this.languageModelManager.findOneOrConfigureOneFor(video.language);
    if (!modelName) {
      this.logger.warn(
        `Couldn't find and setup a language model for video "${video.id}" in "${video.language}". Skipping transcription...`
      );
      return;
    }

    this.workerPool.runTask({
      task: new TranscriptionRequest(video, videoFiles[0]),
      callback: this.createTaskRunnerCallback(video, res, false),
    });
  }

  private createTaskRunnerCallback(
    video: Pick<MVideo, 'id' | 'language'>,
    res: AuthenticatedPeerTubeResponse,
    notifyUrl: string | boolean
  ) {
    return (e?: Error | null, outputFilePath?: string | null) => {
      if (e) {
        this.logger.log({ level: 'error', message: e });
      }

      // if (!e && outputFilePath) {
      //   this.peerTubeApi.setOAuthTokenFromResponse(res);
      //   this.peerTubeApi
      //     .addVideoCaption(video.id, video.language, createReadStream(outputFilePath))
      //     .then(() => this.triggerTranscriptionAndTranslation(video.id, notifyUrl))
      //     .catch((e) => {
      //       this.logger.error(
      //         `Failed to upload caption ${outputFilePath} to video "${video.id}", destroying the stream...`
      //       );

      //       throw e;
      //     });
      // }
    };
  }

  private async getVideoFiles(id: number) {
    return await this.peerTubeVideosHelpers
      .getFiles(id)
      .then(({ webtorrent: { videoFiles: webtorrentVideoFiles }, hls: { videoFiles: hlsVideoFiles } }) =>
        webtorrentVideoFiles.concat(hlsVideoFiles).sort(({ size: sizeA }, { size: sizeB }) => sizeA - sizeB)
      );
  }
  private triggerTranscriptionAndTranslation(
    videoId: number,
    downloadUrl: String,
    language: string | boolean,
    triggerUrl: string | boolean
  ) {
    this.logger.error('Chagai testing: ' + triggerUrl);
    if (!triggerUrl) {
      return;
    }
    let body: { [key: string]: any } = {
      videoId: videoId,
      url: downloadUrl, // change back to downloadUrl TODO
    };
    if (language) {
      body.language = language;
    }

    fetch(`${triggerUrl}/transcribe-and-translate/video`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
      .then((response) => {
        if (!response.ok) {
          response.json().then((json_error) => {
            throw new Error(`HTTP error! status: ${response.status} and message ${json_error.message}`);
          });
        }
        return response.json();
      })
      .then((data) => {
        this.logger.info(`json result from server ${data.success ? 'successful' : 'unsuccessful'}`);
      })
      .catch((error) => {
        this.logger.error(`Failed to report transcription done (for translation) "${error}", skipping.`);
      });
  }
}
