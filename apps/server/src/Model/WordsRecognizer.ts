import { Model, Recognizer } from 'vosk';

export class WordsRecognizer {
  private recognizer: Recognizer<any>;

  constructor(model: Model, sampleRate: number = 16000) {
    this.recognizer = new Recognizer({ model, sampleRate });
    this.recognizer.setWords(true);
  }
}
