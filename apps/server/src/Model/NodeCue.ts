import { Cue, NodeCue as NodeCueInterface } from 'subtitle';
import { WordResult } from 'vosk';

export class NodeCue implements NodeCueInterface {
  public type: 'cue';
  public data: Cue;

  constructor({ start, end, text }: Cue) {
    this.data = {
      start: start * 1000,
      end: end * 1000,
      text,
    };
    this.type = 'cue';
  }

  static createFromWordResults(wordResults: WordResult[]): NodeCueInterface {
    return new NodeCue({
      start: wordResults[0].start,
      end: wordResults[wordResults.length - 1].end,
      text: wordResults.map(({ word }) => word).join(' '),
    });
  }
}
