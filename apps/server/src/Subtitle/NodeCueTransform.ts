import { RecognitionResults, Recognizer } from 'vosk';
import { TransformCallback, Transform } from 'stream';
import { LoggerInterface, WordRecognition, NodeCue } from '../Model';
import { toTimecode } from '../utils';

export class NodeCueTransform extends Transform {
  private wordRecognizer: Recognizer<any>;
  private readonly wordRecognitions: WordRecognition[];
  private readonly silenceThreshold: number;
  private readonly maxWordsPerNode: number = 30;
  private readonly logger: LoggerInterface | undefined;

  constructor(wordRecognizer: Recognizer<any>, silenceThreshold = 0.3, logger?: LoggerInterface) {
    super({ objectMode: true, writableObjectMode: true });
    this.wordRecognizer = wordRecognizer;
    this.silenceThreshold = silenceThreshold;
    this.wordRecognitions = [];
    this.logger = logger;
  }

  _transform(chunk: any, encoding: BufferEncoding, callback: TransformCallback) {
    try {
      if (this.wordRecognizer.acceptWaveform(chunk)) {
        this.pushResults(this.wordRecognizer.result());
      }

      this.pushResults(this.wordRecognizer.finalResult());
      const breakpoint = this.wordRecognitions.findIndex(
        (wordRecognition, index) =>
          wordRecognition.isAboveSilenceThreshold(this.silenceThreshold) || index > this.maxWordsPerNode
      );

      if (breakpoint !== -1) {
        const nodeWordRecognitions = this.wordRecognitions.splice(0, breakpoint + 1);
        if (nodeWordRecognitions.length > 0) {
          if (this.logger) {
            const nodeText = nodeWordRecognitions.map(({ word }) => word).join(' ');

            const start = toTimecode(nodeWordRecognitions[0].start);
            const end = toTimecode(nodeWordRecognitions[nodeWordRecognitions.length - 1].end);
            this.logger.info(nodeText, { start, end });
          }

          callback(null, NodeCue.createFromWordResults(nodeWordRecognitions));
          return;
        }
      }

      callback();
    } catch (e) {
      callback(e as Error);
    }
  }

  _flush(callback: TransformCallback) {
    try {
      this.wordRecognizer.free();
      if (this.wordRecognitions.length > 0) {
        callback(null, NodeCue.createFromWordResults(this.wordRecognitions));
      } else {
        callback();
      }
    } catch (e) {
      callback(e as Error);
    }
  }

  private pushResults({ result: wordResults }: RecognitionResults) {
    if (wordResults) {
      // update the last word recognition of the previous batch
      const l = this.wordRecognitions.length;
      if (l > 0) {
        this.wordRecognitions[l - 1] = new WordRecognition(this.wordRecognitions[l - 1], wordResults[0]);
      }

      // calculate "distance" (silence duration) with the next recognized word
      const wordRecognitions = wordResults.map((wordResult, index) => {
        const nextWordResult = wordResults[index + 1];
        return new WordRecognition(wordResult, nextWordResult);
      });
      this.wordRecognitions.push(...wordRecognitions);
    }
  }
}
