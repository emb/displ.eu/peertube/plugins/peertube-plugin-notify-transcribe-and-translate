import { resolve } from 'path';
import os from 'os';
import { performance, PerformanceObserver } from 'perf_hooks';
import { RegisterServerOptions } from '@peertube/peertube-types/server/types';
import { LanguageModelManager, VoskModelPageParser } from './Recognizer';
import { SettingsRegistry, PeerTubeApi, SettingEntry } from './PeerTube';
import { LanguageModelRepository } from './Repository/LanguageModelRepository';
import { SettingsController, VideoController } from './Controller';
import { TranscriptionWorkerPool } from './Transcription/TranscriptionWorkerPool';
import { toHumanReadable } from './utils';
import { PluginSettingsManagerInterface } from './Model/PluginSettingsManagerInterface';

export async function register({
  registerHook,
  registerSetting,
  peertubeHelpers: {
    logger,
    config: { getWebserverUrl },
    plugin: { getDataDirectoryPath },
    videos: peertubeVideosHelpers,
  },
  storageManager,
  videoLanguageManager,
  settingsManager,
}: RegisterServerOptions & {
  settingsManager: PluginSettingsManagerInterface;
}) {
  logger.debug('Registering');
  const dataPath = getDataDirectoryPath();
  const modelPath = resolve(getDataDirectoryPath(), 'models');

  const availableLanguages = await videoLanguageManager.getConstants();
  const voskModelPageParser = new VoskModelPageParser(
    'https://alphacephei.com/vosk/models/model-list.json',
    logger,
    Object.keys(availableLanguages)
  );
  const languageModelRepository = new LanguageModelRepository(storageManager, voskModelPageParser, logger);
  await languageModelRepository.load();
  const peerTubeApi = new PeerTubeApi(logger, getWebserverUrl());

  registerSetting({
    name: 'trigger-transcription-and-translation',
    label: 'URL to trigger the transcription and translation',
    type: 'input',
    private: true,
  });

  const settingsRegistry = new SettingsRegistry(settingsManager, registerSetting, storageManager, logger);
  const languageModelManager = new LanguageModelManager(
    settingsRegistry,
    languageModelRepository,
    logger,
    modelPath,
    availableLanguages
  );
  await languageModelManager.registerAll();

  const settingsController = new SettingsController(logger, languageModelManager);
  settingsManager.onSettingsChange(settingsController.handleChanges.bind(settingsController));

  const transcriptionPerformanceObserver = new PerformanceObserver((items) => {
    items
      .getEntries()
      .forEach((entry) => logger.debug(`Transcription n°${entry.name} took ${toHumanReadable(entry.duration)}`, entry));
    performance.clearMarks();
  });

  // We consider that one CPU will be required by PeerTube. This will be refined in further versions.
  const nbCpus = os.cpus().length;
  const availableCpus = nbCpus === 1 || nbCpus === 0 ? 0 : nbCpus - 1;
  if (availableCpus <= 0) {
    logger.error(
      `There won't be enough CPUs available (${availableCpus}/${nbCpus}) to process the transcription requests.`
    );
  }

  const pool = new TranscriptionWorkerPool(availableCpus || 1, dataPath, logger, transcriptionPerformanceObserver);
  const videoController = new VideoController(
    logger,
    languageModelManager,
    peerTubeApi,
    peertubeVideosHelpers,
    pool,
    settingsManager
  );

  registerHook({
    target: 'action:api.video.updated',
    handler: videoController.updated.bind(videoController),
  });

  registerHook({
    target: 'action:api.video.uploaded',
    handler: videoController.uploaded.bind(videoController),
  });

  registerHook({
    target: 'action:api.video-caption.deleted',
    handler: videoController.captionDeleted.bind(videoController),
  });

  const instanceLanguages = await peerTubeApi.getInstanceLanguages();
  await Promise.all(instanceLanguages.map((language) => languageModelManager.findOneOrConfigureOneFor(language)));
}

export async function unregister() {
  return;
}
