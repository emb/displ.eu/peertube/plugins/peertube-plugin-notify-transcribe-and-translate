## Summary

<!-- Summarize the bug encountered concisely -->

- __plugin__ version:
- __PeerTube__ version:

## Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

1. 

## What is the current bug behavior?

<!-- What actually happens -->

## What is the expected correct behavior?

<!-- What you should see instead -->

## Relevant logs and/or screenshots

<!--
  Paste any relevant logs - please use code blocks (```) to format console output, logs, and code,
  as it's very hard to read otherwise.
  Depending on your installation relevant logs might be found:
    - Administration > System > Logs
    - `cat /var/www/peertube/storage/logs/peertube.log | grep peertube-plugin-transcription`
    - ...
-->

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->
