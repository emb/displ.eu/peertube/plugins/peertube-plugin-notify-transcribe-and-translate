
## Pitfalls

- `ffmpeg fails with: Unable to find a suitable output format for 'pipe:'`
https://stackoverflow.com/questions/23687485/ffmpeg-fails-with-unable-to-find-a-suitable-output-format-for-pipe


## Resources

- Limiting the output bit rate with `bufsize`
https://trac.ffmpeg.org/wiki/Limiting%20the%20output%20bitrate
